`timescale 1ns / 1ps

module ram #(parameter address = 10, width = 16,  word = 2) ( CLK, addr, write, dato_in, dato_out, palabra );

   input CLK;
   input write;                                                               
   input [address-1:0 ] addr;                                                      
   input [word-1:0] palabra;
   input [width-1 : 0] dato_in; 
   output logic [width -1 : 0] dato_out;
   
 
   
   reg [width-1 : 0][2**word-1:0] ram[0 : 2**address-1]; 
   
   initial
   begin
   $readmemb("Ram.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[addr][palabra]   <= dato_in;
   // ram[9:0][2:0]
   
   end
   
   always @(posedge CLK) 
   
   begin
   if(write == 0)
   dato_out <= ram[addr][palabra];
   end
   
   
   
   
   
endmodule