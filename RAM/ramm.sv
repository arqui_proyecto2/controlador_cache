`timescale 1ns / 1ps

`timescale 1ns / 1ps


module ram #(parameter address = 13, width = 64) ( CLK, addr, write, dato_in, dato_out);
   input CLK;
   input write;
   input [address-1:0 ] addr;

   input [width-1 : 0] dato_in; 
   output logic [width -1 : 0] dato_out;
   
   reg [width-1 : 0] ram[0 : 2**address-1];
   
   initial
   begin
   $readmemb("Ram2.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[addr]   <= dato_in;   
   end
   
   always @(posedge CLK)   
   begin
   if(write == 0)
   dato_out <= ram[addr];
   end
   
endmodule

/*                       //8192 = 8 kB
module ram #(parameter address = 13, width = 64,  word = 0) ( CLK, addr, palabra, write, dato_in, dato_out);
   
   input CLK;
   input write;                                                               
   input [address-1:0 ] addr;                                                      
   input [word-1:0] palabra;
   input [width-1 : 0] dato_in; 
   output logic [width -1 : 0] dato_out;
   
   
   //Cuantas palabras//Tamaño de palabra
   reg [2**word-1:0][width-1 : 0] ram[0 : 2**address-1];
   
   initial
   begin
   $readmemb("Ram2.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[addr][palabra]   <= dato_in;
   // ram[9:0][2:0]
   
   end
   
   always @(posedge CLK) 
   
   begin
   if(write == 0)
   dato_out <= ram[addr][palabra];
   end
   
endmodule*/