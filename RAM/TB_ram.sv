`timescale 1ns / 1ps

module TB_ram();
    
    parameter address = 10;
    parameter width = 16;
    parameter word = 2;
    
    logic CLK;
    logic write;
    logic [address-1:0] addr;
    logic [width-1:0] dato_in;
    logic [word-1:0 ] palabra;
    logic[width-1:0] dato_out;
    
    ram #(address, width, word)  ram_unit( CLK, addr, write, dato_in, dato_out , palabra);
    
    
    always 
    begin
    #10 CLK = 1;
    #10 CLK = 0;
    
    end
    
    
    
    
    initial 
    
    begin
//    write = 1'b1;
//    #10;
//    addr = 10'b0000000000;
//    palabra = 3'b000;
//    dato_in = 8'b00100100;
//    #10;
//    #10;
    /////////////////////////////////////////////////
    #20;
    write = 1'b0;
    addr = 10'b0000000000;
    palabra = 2'b00;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000000;
    palabra = 2'b01;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000000;
    palabra = 2'b10;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000000;
    palabra = 2'b11;
    dato_in = 16'b0000000000000000;
    
    /////////////////////////////////////////////////////
    #20;
    write = 1'b0;
    addr = 10'b0000000001;
    palabra = 2'b00;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000001;
    palabra = 2'b01;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000001;
    palabra = 2'b10;
    dato_in = 16'b0000000000000000;
    
    #20;
    write = 1'b0;
    addr = 10'b0000000001;
    palabra = 2'b11;
    dato_in = 16'b0000000000000000;
    
    
    //////////////////////////////////////////////////
    
    end
    
    

endmodule