`timescale 1ns / 1ps

module Sim_FSM1();

    logic CLK, reset, run, R_W, H_M, DR, DRM;
    logic [2:0] output1;

    FSM_Controller TB_FSM1(.CLK(CLK), .run(run), .reset(reset), .R_W(R_W), .H_M(H_M), .DR(DR), .DRM(DRM), .output1(output1));
    
    always 
    begin
    #10 CLK = 1;
    #10 CLK = 0;
    end
    
    initial 
    begin
    
    #10;            //Estado reposo                 (1)
    run = 1'b0;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b1;
    DRM = 1'b1;
    
    #20;            //Estado Leer Cach�     11XXX    (2)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b0;
    DR = 1'b1;
    DRM = 1'b0;

    #20;            //Estado Lectura 11100          (3)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b0;
    DRM = 1'b0;
    
    #20;            //Estado Reposo 01110           (1)
    run = 1'b0;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b1;
    DRM = 1'b0;

    #20;            //Estado Leer cach� 11XXX       (2)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b0;
    DRM = 1'b0;    
    
    #20;           //Estado Busqueda ram 11000           (4)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b0;
    DR = 1'b0;
    DRM = 1'b0;
    
    #20;            //Escritura  10001                (5)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b0;
    H_M = 1'b0;
    DR = 1'b0;
    DRM = 1'b1;
    
    #20;            //lectura   11001               (3)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b0;
    DR = 1'b0;
    DRM = 1'b1;

   #20;            //Estado Reposo 01110           (1)
    run = 1'b0;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b1;
    DRM = 1'b0;
    
    #20;            //Escritura  10001                (5)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b0;
    H_M = 1'b0;
    DR = 1'b0;
    DRM = 1'b1;
    
    #20;            //lectura   11001               (3)
    run = 1'b1;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b0;
    DR = 1'b0;
    DRM = 1'b1;
    
   #20;            //Estado Reposo 01110           (1)
    run = 1'b0;
    reset = 1'b0;
    R_W = 1'b1;
    H_M = 1'b1;
    DR = 1'b1;
    DRM = 1'b0;    
        
    end
endmodule