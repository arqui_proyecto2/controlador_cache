`timescale 1ns / 1ps
                                    //R_W = read/write, H_M = Hit/Miss, DR = DataReady, DRM = DataReadyMain
module FSM_Controller(
    input logic CLK,
    input logic reset, run, R_W, H_M, DR, DRM,
    output logic[2:0] output1     
    );
  
  
    
       enum logic [2:0] {state1 = 3'b000,
                         state2 = 3'b001,
                         state3 = 3'b010,
                         state4 = 3'b011,
                         state5 = 3'b100,
                         state6 = 3'b101,
                         state7 = 3'b110,
                         state8 = 3'b111} state;
    
       always @(posedge CLK)
          if (reset)
             state <= state1;
          else
             case (state)
                state1 : begin
                   if (run == 1'b1 && R_W == 1'b1)
                      state <= state2;
                   else if (run == 1'b1 && R_W == 1'b0)
                      state <= state5;
                   else
                      state <= state1;
                end
                state2 : begin
                   if (run == 1'b1 && R_W == 1'b1 && H_M == 1'b1)
                      state <= state3;
                   else if (run == 1'b1 && R_W == 1'b1 && H_M == 1'b0)
                      state <= state4;
                   else
                      state <= state2;
                end
                state3 : begin
                   if (run == 1'b0 && R_W == 1'b1 && H_M == 1'b1 && DR == 1'b1)
                      state <= state1;
                   else
                      state <= state3;
                   end
                state4 : begin
                   if (run == 1'b1 && R_W == 1'b0 && H_M == 1'b0 && DRM == 1'b1)
                      state <= state5;
                   else
                      state <= state4;
                end
                state5 : begin
                   if (run == 1'b1 && R_W == 1'b1 && DRM == 1'b1)
                      state <= state3;
                   else
                      state <= state5;
                end
                default : begin  // Fault Recovery
                   state <= state1;
                end
             endcase
    
       assign output1 = state;
    
endmodule
