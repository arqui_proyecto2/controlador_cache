`timescale 1ns / 1ps

module Controlador(
    input logic CLK,
    input logic reset, run, R_W, H_M, DR, DRM,
    output logic [7:0] Salida     
    );
    
    wire [2:0] State;
            
    FSM_Controller FSM_Controller(.CLK(CLK),.reset(reset), .run(run), .R_W(R_W), .H_M(H_M), .DR(DR), .DRM(DRM), .output1(State));
    
    Deco_FSM_Controller Deco_FSM_Controller(.Estado(State), .SalidaDeco(Salida));
      
endmodule
