`timescale 1ns / 1ps

module Deco_FSM_Controller(
    input logic [2:0] Estado,  
    output logic [7:0] SalidaDeco
    );


always_comb                // 8'b    000     0     0              0          0           0
	case (Estado)          //                             W_fromMain(miss) |S_RAM(miss)| PD     //  S_RAM = Se�al de busqueda en ram
	3'b000:        //reposo
		   SalidaDeco = 8'b00000000;
	3'b001:        // leer cache 
		   SalidaDeco = 8'b00000000;		
	3'b010:        // Lectura
		   SalidaDeco = 8'b00000000;
	3'b011:                         
           SalidaDeco = 8'b00000000;
	3'b100:
           SalidaDeco = 8'b00000000;
	3'b101:
           SalidaDeco = 8'b00000000;
	3'b110:
           SalidaDeco = 8'b00000000;
	3'b111:
           SalidaDeco = 8'b00000000;

	default:
		   SalidaDeco = 8'b00000000;		
	endcase

//cambiar salidas
assign A = SalidaDeco[0];
assign B = SalidaDeco[1];
assign C = SalidaDeco[2];
assign D = SalidaDeco[3];
assign E = SalidaDeco[4];
assign F = SalidaDeco[5];
assign G = SalidaDeco[6];
assign H = SalidaDeco[7];

endmodule