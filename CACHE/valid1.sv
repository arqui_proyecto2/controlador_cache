`timescale 1ns / 1ps

module valid1 #(parameter setNum = 10, width = 1) ( CLK, set, write, dato_out);

   input CLK;
   input write;
   input [setNum-1:0 ] set;
   output logic [width -1 : 0] dato_out;
   
 
   
   logic [width-1 : 0] ram[0 : 2**setNum-1];
   
   initial
   begin
   $readmemb("valid1.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[setNum]   <= 1'b1;
   
   
   end
   
   always @(posedge CLK) 
   
   begin
   if(write == 0)
   dato_out <= ram[setNum];
   end
   
   
   
endmodule
