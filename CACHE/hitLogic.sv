`timescale 1ns / 1ps

//PARA LOS 4 MÓDULOS DE WAY   // 4-AZUL, 4-VERDE, 1-CELESTE

module hitLogic #(parameter N = 1)(tagDir, tag_way1, tag_way2, tag_way3, tag_way4, valid1, valid2, valid3, valid4,
hit1, hit2, hit3, hit4, HIT);

//Primero se comparan ambas etiquetas y la salida de estas es una entrada de la AND.
//La otra entrada es el valid bit. 
//N indica la cantidad de bits de cada entrada de la AND.
 
input valid1, valid2, valid3, valid4 ;
input [35:0] tagDir, tag_way1, tag_way2, tag_way3, tag_way4;

output hit1, hit2, hit3, hit4, HIT;


wire outC1, outC2, outC3, outC4;

ComparadorDeEtiquetas comp1( .in1(tagDir), .in2(tag_way1), .outC(outC1) );
ComparadorDeEtiquetas comp2( .in1(tagDir), .in2(tag_way2), .outC(outC2) );
ComparadorDeEtiquetas comp3( .in1(tagDir), .in2(tag_way3), .outC(outC3) );
ComparadorDeEtiquetas comp4( .in1(tagDir), .in2(tag_way4), .outC(outC4) );

AND #( N ) an1(valid1, outC1, hit1);
AND #( N ) an2(valid2, outC2, hit2);
AND #( N ) an3(valid3, outC3, hit3);
AND #( N ) an4(valid4, outC4, hit4);

OR #(1)  oHit(hit1, hit2, hit3, hit4,
HIT );


endmodule
