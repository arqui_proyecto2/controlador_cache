`timescale 1ns / 1ps

module CacheRam2 #(parameter address = 10, width = 16,  word = 2) ( CLK, addr, palabra, write, dato_in, dato_out);

   input CLK;
   input write;                                                               
   input [address-1:0 ] addr;                                                      
   input [word-1:0] palabra;
   input [width-1 : 0] dato_in; 
   output logic [width -1 : 0] dato_out;
   
 
   //Cuantas palabras//Tamaño de palabra
   reg [2**word-1:0][width-1 : 0] ram[0 : 2**address-1];  
   
   initial
   begin
   $readmemb("way2.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[addr][palabra]   <= dato_in;
   // ram[9:0][2:0]
   
   end
   
   always @(posedge CLK) 
   
   begin
   if(write == 0)
   dato_out <= ram[addr][palabra];
   end
  
endmodule

/*module CacheRam2 #(parameter setNum = 10, width = 8) ( CLK, set, write, dato_in, dato_out);

   input CLK;
   input write;
   input [setNum-1:0 ] set;

   input [width-1 : 0] dato_in; 
   output logic [width -1 : 0] dato_out;
   
   logic [width-1 : 0] ram[0 : 2**setNum-1];
   
   initial
   begin
   $readmemb("way1.mem", ram);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   ram[setNum]   <= dato_in;
   
   
   end
   
   always @(posedge CLK) 
   
   begin
   if(write == 0)
   dato_out <= ram[setNum];
   end
      
endmodule*/
