`timescale 1ns / 1ps

module TB_cache();
    //module cache (address, dato_in, CLK, write, dato_out, HIT);    
    
    logic CLK;
    logic write;
    logic [47:0] address;
    logic [35:0] tag_in;
    logic [9:0] set_in;
    logic [1:0] word_in;
    logic [15:0] dato_in;
 
    logic [15:0] dato_out;
    logic HIT;
    
    assign tag_in = address [47:12];
    assign set_in = address [11:2];
    assign word_in = address [1:0];
    
    cache #(1) TB_cache (address, dato_in, CLK, write, dato_out, HIT);
    always 
    begin
    CLK = 1;
    #10; 
    CLK = 0;
    #10;
    end
    
    initial 
    begin
                
    address [47:12] = 36'd1;
    address [11:2]  = 10'd1;
    address [1:0]   = 2'd3;
    dato_in = 16'h5555; 
    write   = 0;    
    #20;
                
    address [47:12] = 36'd1;
    address [11:2]  = 10'd1;
    address [1:0]   = 2'd3;
    dato_in = 16'h5555; 
    write   = 1;    
    #20;
                
    address [47:12] = 36'd2;
    address [11:2]  = 10'd1;
    address [1:0]   = 2'd3;
    dato_in = 16'h5555; 
    write   = 0;    
    #20;
                
    address [47:12] = 36'd1;
    address [11:2]  = 10'd1;
    address [1:0]   = 2'd3;
    dato_in = 16'h5555; 
    write   = 0;    
    #20;
    
    end
    
    

endmodule