`timescale 1ns / 1ps

module MuxCache #(parameter N = 16)(D0, D1, D2, D3,  sel, salida);

input [N-1:0] D0, D1, D2, D3;
input [2:0] sel;

output logic [N-1:0] salida;


always @(sel, D0, D1, D2, D3) 
begin

case(sel)
3'b100: salida = D0;
3'b101: salida = D1;
3'b110: salida = D2;
3'b111: salida = D3;
default salida = 16'hEEEE;


endcase
end
endmodule
