`timescale 1ns / 1ps

            //de CPU
module cache (address, dato_in, CLK, write, dato_out, HIT);
//tagDir, tag_ways4, tag_way2 valid14, hit4, HIT);

input [47:0] address;
input [15:0] dato_in;
input CLK;
input write;
output [15:0] dato_out;
output HIT;

wire [35:0] tag_way1, tag_way2, tag_way3, tag_way4;
wire valid1, valid2, valid3, valid4;
wire hit1, hit2, hit3, hit4;
wire [1:0] selMuxDatoOut;
wire [2:0] selMuxDatoOut2;
wire [15:0] dato_out1, dato_out2, dato_out3, dato_out4;

//Parámetros: 10=bits_para_set; 8=ancho_de_dato; 11=ancho_de_tag; 8=ancho_de_dato; 1=bit_valido;
                                       //SET                          //TAG_CPU      //WORD
way1 #( 10,  16,  36, 1) way1 (CLK, address[11:2], write, dato_in, address[47:12], address[1:0], 
                                dato_out1, tag_way1, valid1);
way2 #( 10,  16,  36, 1) way2 (CLK, address[11:2], write, dato_in, address[47:12], address[1:0], 
                                dato_out2, tag_way2, valid2);
way3 #( 10,  16,  36, 1) way3 (CLK, address[11:2], write, dato_in, address[47:12], address[1:0], 
                                dato_out3, tag_way3, valid3);
way4 #( 10,  16,  36, 1) way4 (CLK, address[11:2], write, dato_in, address[47:12], address[1:0], 
                                dato_out4, tag_way4, valid4);

hitLogic #(1) hitLog(address[47:12] , tag_way1, tag_way2, tag_way3, tag_way4, valid1, valid2, valid3, valid4,
                                hit1, hit2, hit3, hit4, HIT);

HitCache dec(hit1, hit2, hit3, hit4, 
                                selMuxDatoOut);  //REVISAR

assign selMuxDatoOut2 = {HIT, selMuxDatoOut};

MuxCache #(16) muxCache(dato_out1, dato_out2, dato_out3, dato_out4, selMuxDatoOut2, 
                                dato_out);
endmodule