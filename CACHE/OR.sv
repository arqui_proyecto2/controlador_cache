`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2019 00:48:44
// Design Name: 
// Module Name: OR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OR #(parameter N = 1) (entrada1, entrada2, entrada3, entrada4,
salida );
input [N-1:0] entrada1, entrada2, entrada3, entrada4;
output [N-1:0] salida;


assign salida = entrada1  | entrada2  | entrada3  | entrada4;

endmodule

