`timescale 1ns / 1ps

module ComparadorDeEtiquetas( input [35:0] in1, input [35:0] in2, output logic outC );
  always @ ( in1 or in2 )
  begin
    if ( in1 == in2) 
    begin
      outC = 1'b1;
      end
    else
      begin
          outC = 1'b0;
      end
end
endmodule

