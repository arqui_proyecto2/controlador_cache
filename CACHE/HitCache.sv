`timescale 1ns / 1ps

module HitCache(hit1, hit2, hit3, hit4, selMuxDatoOut);

input hit1, hit2, hit3, hit4;
output logic [1:0] selMuxDatoOut;

always_comb
begin
case({hit1, hit2, hit3, hit4})

4'b0001: selMuxDatoOut = 2'b00;
4'b0010: selMuxDatoOut = 2'b01;
4'b0100: selMuxDatoOut = 2'b10;
4'b1000: selMuxDatoOut = 2'b11;
default selMuxDatoOut = 2'b00;

endcase



end

 
endmodule
