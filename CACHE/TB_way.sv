`timescale 1ns / 1ps

module TB_way();
    //(CLK, set(10), write, dato_in(16), tag_in(36), word (2), dato_out(16), tag_out(36), valid);
    //parameter setNum = 10, width = 16, tag_length = 36, vBit = 1
    parameter setNum = 10;
    parameter width = 16;
    parameter tag_length = 36;
    parameter vBit = 1;
    
    logic CLK, write, valid;
    logic [width-1:0] dato_in, dato_out;
    logic [tag_length-1:0] tag_in, tag_out;
    logic [setNum-1:0] addr; //set
    logic [1:0] word;
    
    way1 #(setNum, width, tag_length, vBit)  wayway1( CLK, addr, write, dato_in, tag_in, word, 
                                                      dato_out, tag_out, valid);
    
    always 
    begin
    #10 CLK = 1;
    #10 CLK = 0;
    
    end
  
    initial    
    begin
        #10;
        addr = 10'b0000000000;
        write = 1'b0;
        dato_in = 16'b1010101010101010;
        tag_in = 36'b000000000000000000000000000000000000;
        word = 2'b00;
        
        #20;  //LEE LA POSICIÓN 0:000000000
        addr = 10'b0000000000;
        write = 1'b0;
        dato_in = 16'b1010101010101010;
        tag_in = 36'b000000000000000000000000000000000000;
        word = 2'b00;
        
        #20;  //LEE EN LA POSICIÓN 0:000000001
        addr = 10'b0000000001;
        write = 1'b0;
        dato_in = 16'b1010101010101010;
        tag_in = 36'b000000000000000000000000000000000000;
        word = 2'b00;
        
        #20;  //ESCRIBE EN LA POSICIÓN 0:000000000
        addr = 10'b0000000000;
        write = 1'b1;
        dato_in = 16'b1010101010101010;
        tag_in = 36'b000000000000000000000000000000000000;
        word = 2'b00;
        
        #20;  //LEE EN LA POSICIÓN 0:000001000
        addr = 10'b0000001000;
        write = 1'b0;
        dato_in = 16'b1010101010101010;
        tag_in = 36'b000000000000000000000000000000000000;
        word = 2'b00;
        
    end

endmodule