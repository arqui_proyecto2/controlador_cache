`timescale 1ns / 1ps


module tags4 #(parameter address = 36, width =11) ( CLK, addr, write, tag_in, tag_out);
   input CLK;
   input write;
   input [address-1:0 ] addr;

   input [width-1 : 0] tag_in; 
   output logic [width -1 : 0] tag_out;
   
   reg [width-1 : 0] tag[0 : 2**address-1];
   
   initial
   begin
   $readmemb("tags4.mem", tag);
   end
   
   always @(posedge CLK)
   
   begin
   
   if( write == 1)
   tag[addr]   <= tag_in;   
   end
   
   always @(posedge CLK)   
   begin
   if(write == 0)
   tag_out <= tag[addr];
   end
   
endmodule
