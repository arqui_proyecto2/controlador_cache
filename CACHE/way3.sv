`timescale 1ns / 1ps

module way3 #( parameter setNum = 10, width = 16, tag_length = 36, vBit = 1)

    (CLK, set, write, dato_in, tag_in, word, dato_out, tag_out, valid);

    input CLK;
    input write;
    input [setNum-1:0 ]set;
    input [1:0] word;
    input [width-1:0] dato_in;
    input [tag_length-1 : 0 ] tag_in;
    
    output [width -1 : 0] dato_out;
    output [tag_length-1 : 0 ] tag_out;
    output valid;
    
   CacheRam3 #(setNum , width ) word3 ( CLK, set, word, write, dato_in, dato_out); 
    
   tags3 #( setNum , tag_length) tagArray3 ( CLK, set, write, tag_in, tag_out);
    
   valid3 #(setNum , vBit)  validarray3 ( CLK, set, write, valid);
    
endmodule
