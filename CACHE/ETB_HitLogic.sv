`timescale 1ns / 1ps

module ETB_HitLogic( );    

    logic [35:0] tagDir=0, tag_way1=0, tag_way2=0, tag_way3=0, tag_way4=0;
    logic valid1=0, valid2=0, valid3=0, valid4=0;
    logic hit1, hit2, hit3, hit4, HIT, CLK;
    
    hitLogic #(1) hitTB (tagDir, tag_way1, tag_way2, tag_way3, tag_way4, 
                                       valid1, valid2, valid3, valid4,
                                       hit1, hit2, hit3, hit4, HIT);
    always 
    begin
    #10 CLK = 1;
    #10 CLK = 0;
    end
    
    initial 
    begin
    
    #10;
    tagDir   = 36'b000000000000000000000000000000000010;
    tag_way1 = 36'b000000000000000000000000000000000010;
    tag_way2 = 36'b000000000000000000000000000000000000;
    tag_way3 = 36'b000000000000000000000000000000000000;
    tag_way4 = 36'b000000000000000000000000000000000000;
    valid1 = 1'b1;
    valid2 = 1'b1;
    valid3 = 1'b1;
    valid4 = 1'b1;
    
    #20;
    tagDir   = 36'b000000000000000000000000000000000010;
    tag_way1 = 36'b000000000000000000000000000000000010;
    tag_way2 = 36'b000000000000000000000000000000000000;
    tag_way3 = 36'b000000000000000000000000000000000000;
    tag_way4 = 36'b000000000000000000000000000000000000;
    valid1 = 1'b0;
    valid2 = 1'b1;
    valid3 = 1'b1;
    valid4 = 1'b1;
        
    #20;
    tagDir   = 36'b000000000000000000000000000000000100;
    tag_way1 = 36'b000000000000000000000000000000000100;
    tag_way2 = 36'b000000000000000000000000000000000100;
    tag_way3 = 36'b000000000000000000000000000000000000;
    tag_way4 = 36'b000000000000000000000000000000000000;
    valid1 = 1'b1;
    valid2 = 1'b1;
    valid3 = 1'b1;
    valid4 = 1'b1;
    
    #20;
    tagDir   = 36'b000000000000000000000000000000000010;
    tag_way1 = 36'b000000000000000000000000000000000000;
    tag_way2 = 36'b000000000000000000000000000000000000;
    tag_way3 = 36'b000000000000000000000000000000000000;
    tag_way4 = 36'b000000000000000000000000000000000000;
    valid1 = 1'b1;
    valid2 = 1'b1;
    valid3 = 1'b1;
    valid4 = 1'b1;   
    
    end
    
    

endmodule